package corso.lez17.HibernateOneToMany;

import org.hibernate.Session;

import corso.lez17.HibernateOneToMany.models.Carta;
import corso.lez17.HibernateOneToMany.models.Persona;
import corso.lez17.HibernateOneToMany.models.db.GestoreSessioni;

public class App 
{
    public static void main( String[] args )
    {

    	Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();
    	
    	//INSERIMENTO
//    	Persona gio = new Persona("Giovanni", "Pace", "PCAGNN");
//    	
//    	Carta crUno = new Carta("Conad", "CND1234");
//    	crUno.setProprietario(gio);
//    	
//    	Carta crDue = new Carta("Coop", "COO1234");
//    	crDue.setProprietario(gio);
//    	
//    	try {
//    		sessione.beginTransaction();
//    		
//    		sessione.save(gio);
//    		sessione.save(crUno);
//    		sessione.save(crDue);
//    		
//    		sessione.getTransaction().commit();
//    	} catch (Exception e) {
//    		System.out.println(e.getMessage());
//    	} finally {
//			sessione.close();
//		}
    	
    	//RICERCA

    	try {
    		sessione.beginTransaction();
    		
//    		Carta tempCar = sessione.get(Carta.class, 2);
//    		System.out.println(tempCar.stampaDettaglioCarta());
    		
    		Persona tempProp = sessione.get(Persona.class, 1);
//    		System.out.println(tempProp.stampaDettaglioPersona());
    		
    		sessione.getTransaction().commit();
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
    	} finally {
			sessione.close();
		}
    	
    
    }
}
