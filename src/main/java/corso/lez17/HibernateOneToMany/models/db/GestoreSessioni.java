package corso.lez17.HibernateOneToMany.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez17.HibernateOneToMany.models.Carta;
import corso.lez17.HibernateOneToMany.models.Persona;

public class GestoreSessioni {
	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;

	public static GestoreSessioni getIstanza() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_cf.cfg.xml")
					.addAnnotatedClass(Persona.class)
					.addAnnotatedClass(Carta.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
}
